#!/bin/sh

PHP_VERSION=8.2
REPO_NAME=lucidmodules/composer-php-$PHP_VERSION
COMPOSER_VERSION=2.6.2
IMAGE_TAG=$COMPOSER_VERSION

docker pull $REPO_NAME:$IMAGE_TAG
docker tag $REPO_NAME:$IMAGE_TAG $REPO_NAME:latest
docker image push $REPO_NAME:latest
