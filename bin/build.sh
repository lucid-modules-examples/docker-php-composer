#!/bin/sh

PHP_VERSION=8.2
REPO_NAME=lucidmodules/composer-php-$PHP_VERSION
COMPOSER_VERSION=2.6.2
IMAGE_TAG=$COMPOSER_VERSION

docker build -t $REPO_NAME:$IMAGE_TAG -f docker/php-$PHP_VERSION/Dockerfile docker/php-$PHP_VERSION/
