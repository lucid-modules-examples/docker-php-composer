# Docker Image Lucid Modules Composer PHP
The Docker repository must be created in the Docker Registry prior to push.

## Usage
In your CI script you can execute `composer`, e.g.:
```shell
composer install
```

The script source is from the [official Composer website](https://getcomposer.org/doc/faqs/how-to-install-composer-programmatically.md) with added `--version` flag.

## CI Variables
- CI_REGISTRY_USER
- CI_REGISTRY_PASSWORD - this is the Docker Registry Access Token, not the account password
