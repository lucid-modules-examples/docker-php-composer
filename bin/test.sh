#!/bin/sh

PHP_VERSION=8.2
REPO_NAME=lucidmodules/composer-php-$PHP_VERSION
COMPOSER_VERSION=2.6.2
IMAGE_TAG=$COMPOSER_VERSION

docker run --rm $REPO_NAME:$IMAGE_TAG composer --version
